<?php declare(strict_types=1);

namespace App\Services;

use App\Http\Client;
use Carbon\Carbon;
use Exception;

class ApiService
{
    /**
     * Client for making API requests
     *
     * @var Client
     */
    protected Client $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Get a token for working with API
     *
     * @return string
     * @throws Exception
     */
    public function getToken(): string
    {
        if (isset($_SESSION["token"]) && (Carbon::now()->timestamp - $_SESSION["token_obtained"] < $_ENV["TOKEN_LIFETIME"])) {
            return $_SESSION["token"];
        } else {
            return $this->authorize($_ENV["NAME"], $_ENV["EMAIL"], $_ENV["CLIENT"]);
        }
    }

    /**
     * Obtain an API token from the server
     *
     * @param $name
     * @param $email
     * @param $client_id
     *
     * @return string
     * @throws Exception
     */
    public function authorize($name, $email, $client_id): string
    {
        $_SESSION["token"] = $this->client->register($name, $email, $client_id)->data->sl_token;
        $_SESSION["token_obtained"] = Carbon::now()->timestamp;
        return $_SESSION["token"];
    }

    /**
     * Get posts on the given page
     *
     * @param int $page
     *
     * @return array
     * @throws Exception
     */
    public function getPosts(int $page): array
    {
        return $this->client->getPosts($this->getToken(), $page)->data->posts;
    }
}