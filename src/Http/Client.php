<?php declare(strict_types=1);

namespace App\Http;

use Exception;

class Client
{
    /**
     * Guzzle client for making calls
     * @var \GuzzleHttp\Client
     */
    private \GuzzleHttp\Client $client;

    /**
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(\GuzzleHttp\Client $client)
    {
        $this->client = $client;
    }

    /**
     * Request for a token from API
     *
     * @param string $name
     * @param string $email
     * @param string $client_id
     *
     * @return object
     * @throws Exception
     */
    public function register(string $name, string $email, string $client_id)
    {
        $res = $this->client->post("register",
            ["json" => ["name" => $name, "email" => $email, "client_id" => $client_id]]);
        return $this->checkResponse($res);

    }

    /**
     * Request for posts on the given page from API
     *
     * @param string $token
     * @param int $page
     *
     * @return object
     * @throws Exception
     */
    public function getPosts(string $token, int $page)
    {
        $res = $this->client->get("posts", ["query" => ["sl_token" => $token, "page" => $page]]);
        return $this->checkResponse($res);
    }

    /**
     * Check if response was successful
     *
     * @param $res
     *
     * @return object
     * @throws Exception
     */
    protected function checkResponse($res): object
    {
        if ($res->getStatusCode() === 200) {
            return \GuzzleHttp\json_decode($res->getBody());
        } else {
            throw new Exception("Error on api request: \n".json_encode($res->getBody()), $res->getStatusCode());
        }
    }
}
