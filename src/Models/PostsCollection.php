<?php declare(strict_types=1);

namespace App\Models;

class PostsCollection
{
    /**
     * @var Post[]
     */
    private array $posts = [];

    /**
     * @param array $posts Array of objects representing posts
     */
    public function __construct($posts = [])
    {
        $this->setPosts($posts);
    }

    /**
     * Get posts entities
     * @return Post[]
     */
    public function getPosts(): array
    {
        return $this->posts;
    }

    /**
     * Save new posts
     * @param array $posts Array of objects representing posts
     * @return void
     */
    public function setPosts(array $posts): void
    {
        $this->posts = [];
        if (sizeof($posts)) {
            $this->addPosts($posts);
        }
    }

    /**
     * Add new posts
     * @param array $posts Array of objects representing posts
     * @return void
     */
    public function addPosts(array $posts): void
    {
        if (sizeof($posts)) {
            foreach ($posts as $post) {
                $this->posts[] = new Post($post);
            }
        }
    }
}