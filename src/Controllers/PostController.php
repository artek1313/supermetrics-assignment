<?php declare(strict_types=1);

namespace App\Controllers;

use App\Models\PostsCollection;
use App\Services\ApiService;
use Exception;

class PostController
{
    /**
     * @var ApiService
     */
    protected ApiService $service;

    /**
     * @param ApiService $service
     */
    public function __construct(ApiService $service)
    {
        $this->service = $service;
    }

    /**
     * Get asked number of post pages from API
     *
     * @param int $pages
     *
     * @return PostsCollection
     * @throws Exception
     */
    public function fetchPosts(int $pages)
    {
        $posts = new PostsCollection();
        for ($i = 0; $i < $pages; $i++) {
            $posts->addPosts($this->service->getPosts($i));
        }
        return $posts;
    }
}