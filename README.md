# README #

### Why? ###

* This solution is made for calculating fictional Supermetrics Social Network posts stats

### How? ###

* enter project directory `cd supermetrics-assignment`
* run `composer install` to install dependencies
* copy example environment `cp .env.example .env`
* put your authorization api data in the `.env` file
* run it `php index.php`

### Who? ###

* Denis Zavialkin