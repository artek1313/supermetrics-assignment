<?php declare(strict_types=1);

namespace App\Models;

class Post
{
    /**
     * @var string
     */
    protected string $id;

    /**
     * @var string
     */
    protected string $from_name;

    /**
     * @var string
     */
    protected string $from_id;

    /**
     * @var string
     */
    protected string $message;

    /**
     * @var string
     */
    protected string $type;

    /**
     * @var string
     */
    protected string $created_time;

    /**
     * @param object $post
     */
    public function __construct(object $post)
    {
        $this->id = isset($post->id) ? $post->id : "";
        $this->from_name = isset($post->from_name) ? $post->from_name : "";
        $this->from_id = isset($post->from_id) ? $post->from_id : "";
        $this->message = isset($post->message) ? $post->message : "";
        $this->type = isset($post->type) ? $post->type : "";
        $this->created_time = isset($post->created_time) ? $post->created_time : "";
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFromName(): string
    {
        return $this->from_name;
    }

    /**
     * @param string $from_name
     */
    public function setFromName(string $from_name): void
    {
        $this->from_name = $from_name;
    }

    /**
     * @return string
     */
    public function getFromId(): string
    {
        return $this->from_id;
    }

    /**
     * @param string $from_id
     */
    public function setFromId(string $from_id): void
    {
        $this->from_id = $from_id;
    }


    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getCreatedTime(): string
    {
        return $this->created_time;
    }

    /**
     * @param string $created_time
     */
    public function setCreatedTime(string $created_time): void
    {
        $this->created_time = $created_time;
    }

}