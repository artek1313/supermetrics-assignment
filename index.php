<?php declare(strict_types=1);
require 'vendor/autoload.php';

use App\Http\Client;
use App\Services\ApiService;
use App\Controllers\PostController;
use App\Controllers\StatisticsController;

try {
    session_start();

    //load env
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();

    //guzzle client for http requests
    $guzzle = new GuzzleHttp\Client([
        'base_uri' => $_ENV['API']
    ]);

    //custom client for api calls
    $client = new Client($guzzle);

    //api services provides token check and smoother return values
    $service = new ApiService($client);

    //controllers responsible for fetching of all posts and
    $postsController = new PostController($service);
    $statistics = new StatisticsController();

    echo json_encode($statistics->getPostsStats($postsController->fetchPosts(10)));
} catch (Exception $exception) {
//    echo $exception->getTraceAsString();
    echo $exception->getMessage();
}
