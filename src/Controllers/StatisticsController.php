<?php declare(strict_types=1);

namespace App\Controllers;

use App\Models\PostsCollection;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class StatisticsController
{
    /**
     * @param PostsCollection $posts
     * @return array report with necessary stats
     */
    public static function getPostsStats(PostsCollection $posts): array
    {
        $report = array();

        //prepare start and end dates of the report
        $end = Carbon::now();
        $start = $end->copy()->subMonths(5)->startOfMonth();

        //create an array containing last 6 month
        $month_period = array_map(function ($item) {
            return $item->format("Y-m");
        }, CarbonPeriod::create($start, "1 month", $end)->toArray());

        //an array of all weeks for last 6 month named by the date of their Monday
        $week_period = array_map(function ($item) {
            return $item->format("Y-m-d");
        }, CarbonPeriod::create($start->startOfWeek(), "1 week", $end)->toArray());

        //make zero values for report stats
        $report["average_monthly_length"] = array_fill_keys($month_period, 0);
        $report["biggest_monthly_length"] = $report["average_monthly_length"];
        $monthly_posts = $report["average_monthly_length"];
        $report["weekly_posts"] = array_fill_keys($week_period, 0);

        //calc the stats
        foreach ($posts->getPosts() as $post) {
            $date = Carbon::make($post->getCreatedTime());
            $month = $date->format("Y-m");
            $week = $date->copy()->startOfWeek()->format("Y-m-d");
            $len = strlen($post->getMessage());
            $report["average_monthly_length"][$month] += $len;
            $monthly_posts[$month]++;
            $report["biggest_monthly_length"][$month] = $report["biggest_monthly_length"][$month] > $len ? $report["biggest_monthly_length"][$month] : $len;
            $report["weekly_posts"][$week]++;
            $report["monthly_users"][$post->getFromId()] = isset($report["monthly_users"][$post->getFromId()]) ? $report["monthly_users"][$post->getFromId()] + 1 : 1;
        }

        $report["monthly_users"] = array_map(function ($item) {
            return intdiv($item, 6);
        }, $report["monthly_users"]);

        array_walk($report["average_monthly_length"], function (&$item, $key) use ($monthly_posts) {
            if ($monthly_posts[$key]) {
                $item = intdiv($item, $monthly_posts[$key]);
            } else {
                $item = 0;
            }
        });

        return $report;
    }
}